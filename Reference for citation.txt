# Please cite the following publication if you refer to a FishInspector version 1.7 or beyond:

Nöth, J., Busch, W., Tal, T., Lai, C., Ambekar, A., Kießling, T., Scholz, S., 2024. Analysis of vascular disruption in zebrafish embryos as an endpoint to predict developmental toxicity. Archives of Toxicology 98, 537–554.
