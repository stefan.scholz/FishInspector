%% Parameters

% Name of the model
modelName = "Leica_position_Model"; % Avoid spaces in name as they may not be allowed
modelFile = "Z:\FishClassificator\Model.mat";

% Path to the image files
% This can either be a directory or a tabular data file
% If it is a directory, the labels are the subfolders the images are stored in
% If it is a file, the images & labels are read from their respective column
path = "Z:\FishClassificator\Leica images with position information for classification training.xls";
imageColumnName = "Filename";
labelColumnName = "Position";

% Ratio or number validation images per class!
validationNumber = 0.1;

% Training parameters
maxEpochs = 1000;
batchSize = 100;        % For classification training, the batch size can be much higher
validationPatience = 4;

% Final validation result options
validationResultsPath = "Z:\FishClassificator\Test";
% Toggles if images are copied into respective label folders or if only the results table is saved
copyImagesIntoLabelFolder = true;

%% Training code

% Load image data
if isfolder(path)
    % If the given path is a directory, the folder names are used as class labels
    fprintf("Loading images & labels to train from directory: '%s'... ", path);
    imageStore = imageDatastore(path, "IncludeSubfolders", true, "LabelSource", "foldernames");
elseif isfile(path)
    % If the given path is a file path, read file as table (e.g. excel)
    % The first column must contain the image file locations, and the second column the class labels
    fprintf("Loading images to train from table file: '%s'... ", path);
    t = readtable(path);
    files = string(t.(imageColumnName));         % Filenames are read from first column
    labels = categorical(t.(labelColumnName));        % Labels are read from second column
    imageStore = imageDatastore(files, "Labels", labels);
else
    throw(MException("PathNotRecognized", "Path not valid"));
end
fprintf("Found %d images\n", numel(imageStore.Files));

% Partition images into training & validation data
[validationData, trainingData] = imageStore.splitEachLabel(validationNumber);
fprintf("Split data into %d training and %d validation images\n", numel(trainingData.Files), numel(validationData.Files));

% A pretrained network is used to initialize the layer graph
% squeezenet is the smallest pretrained classification model available in Matlab.
% It is much faster to train than the vgg19 network used in the semantic segmentation training.
% If more accuracy is needed, other pretrained classification networks can be used here (e.g. googlenet)
fprintf("Loading pretrained network...\n");
net = squeezenet(); % alternatives: googlenet(), 
% Get the layer graph from the network
if isa(net,'SeriesNetwork')
    lgraph = layerGraph(net.Layers);
else
    lgraph = layerGraph(net);
end

% Extract layers which need to be modified for transfer learning
fprintf("Modify layer graph for transfer training...\n");
[learnableLayer,classLayer] = findLayersToReplace(lgraph);
classes = categories(trainingData.Labels);
numClasses = numel(classes);
% Replace learnable layer with high learning factors
if isa(learnableLayer,'nnet.cnn.layer.FullyConnectedLayer')
    newLearnableLayer = fullyConnectedLayer(numClasses, ...
        'Name','new_fc', ...
        'WeightLearnRateFactor',10, ...
        'BiasLearnRateFactor',10);
elseif isa(learnableLayer,'nnet.cnn.layer.Convolution2DLayer')
    newLearnableLayer = convolution2dLayer(1,numClasses, ...
        'Name','new_conv', ...
        'WeightLearnRateFactor',10, ...
        'BiasLearnRateFactor',10);
end
lgraph = replaceLayer(lgraph,learnableLayer.Name,newLearnableLayer);
% Replace classification layer
% The classification labels are automatically added to this layer during training
newClassLayer = classificationLayer('Name','new_classoutput');
lgraph = replaceLayer(lgraph,classLayer.Name,newClassLayer);

% The image need to be resized as required by the pretrained network
fprintf("Prepare training data...\n");
inputSize = net.Layers(1).InputSize;
augmentedTrainingData = augmentedImageDatastore(inputSize, trainingData, "ColorPreprocessing", "gray2rgb");
augmentedValidationData = augmentedImageDatastore(inputSize, validationData, "ColorPreprocessing", "gray2rgb");

% Setup training options
options = trainingOptions("sgdm", ...
    "ExecutionEnvironment", "auto", ...
    "InitialLearnRate", 1e-4, ...
    "Momentum", 0.9, ...
    "MaxEpochs", maxEpochs, ...
    "MiniBatchSize", batchSize, ...
    "ValidationData", augmentedValidationData, ...
    "ValidationPatience", validationPatience, ...
    "Shuffle", "every-epoch", ...
    "Plots", "training-progress", ...
    "Verbose", true);

% Run the training
fprintf("Start training...\n");
net = trainNetwork(augmentedTrainingData, lgraph, options);
fprintf("Training finished\n");

% Evaluate the network accuracy
fprintf("Evaluating network accuracy... ");
[pred,scores] = classify(net,augmentedValidationData);
validationPred = validationData.Labels;
accuracy = mean(validationPred == pred);
fprintf("%.2f%%\n", accuracy*100);

% Delete old model
if isfile(modelFile)
    fprintf("Deleting old model...\n");
    delete(modelFile);
end
% Save the model to disk
fprintf("Saving model to disk at '%s'...\n", modelFile);
mat = matfile(modelFile, "Writable", true);
mat.name = modelName;
mat.predictionType = "classification";
mat.classNames = string(classes);
mat.organName = "";
mat.imgSize = inputSize;
mat.net = net;

tableFile = fullfile(validationResultsPath, "predictions.xls");
outputLayer = net.Layers({net.Layers.Name} == string(net.OutputNames{1}));
classNames = string(outputLayer.Classes);
if copyImagesIntoLabelFolder
    saveClassificationResultsToDisk(validationData, pred, scores, classNames, tableFile, validationResultsPath);
else
    saveClassificationResultsToDisk(validationData, pred, scores, classNames, tableFile);
end

fprintf("Finished!\n");


